# Instructions

## Download

To download the application use the git clone command. Then navigate into the backend and rcvar folder seperatly.
The rcvar folder holds the frontend while the backend is located in the backend folder.

## How to start the backend

In order to start the backend make sure you are in the backend folder. Create a virtual env with your preferred method (conda or pip)
Then use the following commands. Depending on what env you are using:

`pip install -r requirements.txt`

`conda install --file requirements.txt`

The backend consists of dummy variables. In order to have more variables just uncomment some variables in the app.py file

To start the frontend, navigate into the rcvar folder. Here you need to execute the following commands:

`npm install` or `npm install --force` if the first command throws an error.

Then to start the application use the following command:

`npm start`

It runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Other Frontend Commands

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

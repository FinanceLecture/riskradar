import base64
import io
import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
from flask import Flask, request, jsonify
from flask_cors import CORS



app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "http://localhost:3000", "methods": ["POST"]}})


@app.route('/get_estimation', methods=['POST'])
def handle_post():
    # Access the data in request
    data = request.json  # If the data sent is in JSON format
    var = 34171.36
    expected_value = 18850.39

    fig = plot_VaR((0.7, 0.7, 1.8127*expected_value), var)

    # Return a response
    return jsonify({"message": "Evaluation Successful",
                    "data": {
                        "best_security_measure": {
                            "name": "Security intelligence systems",
                            "value": -0.20824786,
                        },
                        "company_security": 4272.26,
                        "composition": [
                            {
                                "name": "Notification",
                                "value": 7
                            },
                            {
                                "name": "Post Breach Response",
                                "value": 27
                            },
                            {
                                "name": "Detection and Escalation",
                                "value": 33
                            },
                            {
                                "name": "Lost Business Cost",
                                "value": 33
                            }
                        ],
                    "ev_estimator": 10568.16,
                    "ev_model": 18850.39,
                    "max_value": 4730.00,
                    "min_value": 1287.52,
                    "negative": [
                        {
                            "name": "UK",
                            "value": 73
                        },
                        {
                            "name": "Hybrid",
                            "value": 264
                        },
                        {
                            "name": "Training",
                            "value": 57
                        },
                        # {
                        #     "name": "Insurance",
                        #     "value": 56
                        # },
                        # {
                        #     "name": "Identity Access",
                        #     "value": 52
                        # },
                        # {
                        #     "name": "Automated Checks & ML and AI",
                        #     "value": 281
                        # },
                        # {
                        #     "name": "Encryption Technologies",
                        #     "value": 141
                        # },
                        # {
                        #     "name": "Advanced Perimeter Controls",
                        #     "value": 76
                        # }
                    ],
                    "percentage": 2.69,
                    "positive": [
                        {
                            "name": "US",
                            "value": 468
                        },
                        {
                            "name": "Utilities",
                            "value": 206
                        },
                        {
                            "name": "Insurance",
                            "value": 76
                        },
                        # {
                        #     "name": "Pharmaceuticals",
                        #     "value": 94
                        # },
                        # {
                        #     "name": "Industrial",
                        #     "value": 25
                        # },
                        # {
                        #     "name": "Supplier Breach",
                        #     "value": 11
                        # },
                        # {
                        #     "name": "Medium",
                        #     "value": 119
                        # }
                    ],
                    "var": var,
                    "image": fig.decode('utf8').replace("'", '"'),
            }
        }), 200


def plot_VaR(parameters, var):
    # plot the results
    fig, ax = plt.subplots(1, 1)

    x = np.linspace(stats.geninvgauss.ppf(0.001, *parameters), stats.geninvgauss.ppf(0.99, *parameters),
                    num=100)

    # generate a line with x-values determined before and y-values based on distribution
    # lw = linewidth
    # alpha = how much we zoom into the image
    ax.plot(x, stats.geninvgauss.pdf(x, *parameters), color='red', lw=1.5, alpha=1)
    # label=scipy.stats.geninvgauss.name.capitalize() + ' pdf')

    plt.fill_between(
        x=x,
        y1=stats.geninvgauss.pdf(x, *parameters),
        where=(var < x) & (x <= max(x)),
        color="blue",
        alpha=0.4)

    plt.fill_between(
        x=x,
        y1=stats.geninvgauss.pdf(x, *parameters),
        where=(x <= var),
        color="lawngreen",
        alpha=0.6)

    BIGGER_SIZE = 12
    MEDIUM_SIZE = 1
    plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=0.0001)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=0.001)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=BIGGER_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=BIGGER_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)
    # so x-axis label does not get cut off
    #plt.subplots_adjust(bottom=0.15)

    ticks = ax.get_xticks()[1:]
    ax.set_xticks(ticks)
    ax.set_xlabel("Cost (USD)", fontsize=12)
    ax.set_ylabel("Density of Distribution", fontsize=12)
    ax.set_yticks([])
    # rank the highest probability on top
    ax.legend(loc='best', frameon=False)
    right_side = ax.spines["right"]
    right_side.set_visible(False)
    top_side = ax.spines["top"]
    top_side.set_visible(False)


    my_stringIObytes = io.BytesIO()
    plt.savefig(my_stringIObytes, format='jpg')
    my_stringIObytes.seek(0)
    jpgData = base64.b64encode(my_stringIObytes.read())
    # fig.savefig(f'graphics/VaR/VaR_{parameters[2]}.png')
    # plt.show()

    return jpgData

if __name__ == '__main__':
    app.run()

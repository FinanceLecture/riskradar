import { createTheme, useTheme } from '@mui/material/styles';

const ButtonWidth = "40%"

export const theme = createTheme({
    components: {
        // Name of the component
        MuiButton: {
          styleOverrides: {
            // Name of the slot
            root: {
              // Some CSS
              fontSize: '1rem',
              color: 'MediumAquaMarine',
              borderColor: "MediumAquaMarine",
              fontWeight: "bold",
              width: "50%",
              height: "30%",
              minWidth: "90px",
              "&:hover": {
                backgroundColor: "MediumAquaMarine",
                borderColor: "MediumAquaMarine",
                fontWeight: "bold",
                color: "black",
                height: "31%",
                width: "51%"
            }
            },
          },
        },
    }
  });
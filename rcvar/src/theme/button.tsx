import { createTheme, useTheme } from '@mui/material/styles';

export const theme = createTheme({
    components: {
      // Name of the component
      MuiButton: {
        styleOverrides: {
          // Name of the slot
          root: {
            // Some CSS
            fontSize: '1rem',
            color: 'MediumAquaMarine',
            borderColor: "MediumAquaMarine",
            width: "40%",
            minWidth: "90px"
          },
        },
      },
      MuiCard: {
        styleOverrides: {
            root: {
                width: '500px',
                height: '220px',
                display:'flex',
                justifyContent:'center',
                flexDirection: 'column',
            }
        }
      },
      MuiTab: {
        styleOverrides: {
          root: {
            fontWeight: 'bold',
            color: 'white'
          }
        }
      }
    },
  });
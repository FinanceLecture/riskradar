import { createTheme } from '@mui/material/styles';


export const theme = createTheme({
    components: {
        // Name of the component
        MuiCard: {
            styleOverrides: {
                root: {
                    width: '500px',
                    height: '450px',
                    display:'flex',
                    justifyContent:'center',
                    flexDirection: 'column',
                }
            }
          },
    }
  });
import React, { useEffect } from 'react';
import {Navigate, Route, Routes, useLocation} from 'react-router-dom';
import { useReduxState, useSetReduxState } from '../redux/hooks';
import  { NavigationBar } from '../shared/NavigationBar'
import { Home } from '../components/Homepage/Home'
import { CyberSurvey } from '../components/CyberSurvey/CyberSurvey'
import navigationbarReducer from '../redux/reducers/navigationbarReducer';
import { setShow } from "../redux/reducers/navigationbarReducer"

const AppRouter = () => {

    const urls = useReduxState((state) => state.urls);
    const nav = useReduxState((state) => state.nav);
    const dispatch = useSetReduxState()
    const location = useLocation();
    
    useEffect(() => {
        if(window.location.pathname != urls.pdf){
            dispatch(setShow(true))
        }
    })

    // || location.pathname != urls.pdf

    return (
        <div>
            {nav.show ? <NavigationBar/> : null}
            <Routes>
            <Route path={urls.home} element={<Home/>} />
            <Route path={urls.cyberRisk} element={<CyberSurvey/>} />
            <Route path="*" element={<Navigate to={urls.home} />} />
        </Routes>
        </div>
       

    );

}



export default AppRouter


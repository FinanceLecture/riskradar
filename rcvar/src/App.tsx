import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { persistor, store } from './redux/store';
import { PersistGate } from 'redux-persist/integration/react'
import AppRouter from './router/Approuter';
import { ThemeProvider } from '@mui/material';
import {theme} from './theme/button'



function App() {

  return (
        <BrowserRouter>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <AppRouter />
          </PersistGate>
        </Provider>
      </BrowserRouter>    
  );
}

export default App;

import React, { Dispatch, SetStateAction, useContext, useEffect } from 'react'
import '../../styles/cybersurvey.css'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Card, Checkbox, ListItemText, OutlinedInput, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import { AccessTime, OtherHouses } from '@mui/icons-material';
import {theme} from '../../theme/button'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { PropsCheckbox } from './types';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

export const Checkmarks = (props: PropsCheckbox): React.ReactElement  => {

    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    className='griditems-input'
                    variant="outlined">
                    <CardHeader
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.subheader} />
                    <CardContent>
                        <FormControl fullWidth>
                        <InputLabel>{props.config.label}</InputLabel>
                            <Select
                            multiple
                            value={props.currentValue}
                            onChange={(e: SelectChangeEvent<string[]>) => {
                                props.handleChange(props.config.label, e.target.value)
                            }}
                            input={<OutlinedInput label={props.config.title} />}
                            renderValue={(selected) => selected.join(', ')}
                            MenuProps={MenuProps}
                            >
                            {props.config.items.map((item) => (
                                <MenuItem key={item.value} value={item.value}>
                                <Checkbox checked={props.currentValue.indexOf(item.value) > -1} />
                                <ListItemText primary={item.displayValue} />
                                </MenuItem>
                            ))}
                            </Select>
                        </FormControl>
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )


}
import React, { Dispatch, SetStateAction, useContext, useEffect } from 'react'
import '../../styles/cybersurvey.css'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Card, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import { AccessTime } from '@mui/icons-material';
import {theme} from '../../theme/button'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { PropsSelection } from './types';




export const SelectionCard = (props: PropsSelection): React.ReactElement  => {


    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    className='griditems-input'
                    variant="outlined">
                    <CardHeader
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.subheader} />
                    <CardContent>
                        <FormControl fullWidth required={props.config.label == "Required"}>
                            <InputLabel>{props.config.label}</InputLabel>
                            <Select
                                name={props.config.title}
                                value={props.currentValue}
                                label={props.config.label}
                                onChange={(event: SelectChangeEvent<string | number>) => {
                                    props.handleChange(props.config.name, event.target.value)
                                }}
                            >
                            { props.config.items.map((item) =>
                                <MenuItem key={item.value} value={item.value}>{item.displayValue}</MenuItem>)
                            }
                            </Select>

                        </FormControl>
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )


}
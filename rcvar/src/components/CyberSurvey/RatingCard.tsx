import React, { Dispatch, SetStateAction, SyntheticEvent, useContext, useEffect } from 'react'
import '../../styles/cybersurvey.css'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import Rating from '@mui/material/Rating';
import { Box, Card, InputLabel, SelectChangeEvent, Switch, ThemeProvider, Typography } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import {theme} from '../../theme/button'
import GppGoodIcon from '@mui/icons-material/GppGood';
import ShieldOutlinedIcon from '@mui/icons-material/ShieldOutlined';
import { PropsRating } from './types';




export const RatingCard = (props: PropsRating): React.ReactElement  => {

    const [value, setValue] = React.useState<number | null>(2);
    const [hover, setHover] = React.useState(-1);
    const [checked, setChecked] = React.useState(false);
    
    const getLabelText = (value: number) => {
        return `${value} Star${value !== 1 ? 's' : ''}, ${props.config.items[value]}`;
    }

    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    className='griditems-input'
                    variant="outlined">
                    <CardHeader
                        sx={{marginTop: checked ? null : "20px"}}
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.subheader} />
                    <CardContent>
                    <Box
                        sx={{
                            display: 'flex',
                            alignItems: 'center',
                            marginBottom: "20px",
                            marginTop: checked? "-20px": "1px",
                        }}
                        >
                    <Typography>{props.config.switchLabel}</Typography>
                         <Switch
                        checked={checked}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            props.handleChange(props.config.label, event.target.checked ? (
                                value ? value : 0
                            ) : 0)
                            setChecked(event.target.checked);

                            if(checked){
                                props.handleChange(props.config.label, "No Specification")
                            } else {
                                props.handleChange(props.config.label, 2)
                            }
                        }}
                        inputProps={{ 'aria-label': 'controlled' }}
                        />
                    </Box>
                    { checked ?
                     <Box
                        sx={{
                            width: 250,
                            display: 'flex',
                            alignItems: 'center',
                        }}
                        >
                        <Rating
                            name="hover-feedback"
                            value={value}
                            precision={0.5}
                            getLabelText={getLabelText}
                            onChange={(event: SyntheticEvent<Element, Event>, value: number | null) => {
                                setValue(value)
                                props.handleChange(props.config.label, value ? value : 0)
                              
                            }}
                            onChangeActive={(event: SyntheticEvent<Element, Event>, value: number) => {
                                setHover(value)
                            }}
                            icon={<GppGoodIcon fontSize="inherit" />}
                            emptyIcon={<ShieldOutlinedIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
                        />
                        {value !== null && (
                            <Box sx={{ ml: 2 }}>{props.config.items[hover !== -1 ? hover : value]}</Box>
                        )}
                        </Box>
                        :  null}
                        
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )


}
import { AccessTime } from '@mui/icons-material';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import PublicIcon from '@mui/icons-material/Public';
import FactoryIcon from '@mui/icons-material/Factory';
import ProductionQuantityLimitsIcon from '@mui/icons-material/ProductionQuantityLimits';
import GroupIcon from '@mui/icons-material/Group';
import CloudSyncRoundedIcon from '@mui/icons-material/CloudSyncRounded';
import SchoolRoundedIcon from '@mui/icons-material/SchoolRounded';
import ConnectWithoutContactRoundedIcon from '@mui/icons-material/ConnectWithoutContactRounded';
import HistoryEduRoundedIcon from '@mui/icons-material/HistoryEduRounded';
import QrCodeScannerRoundedIcon from '@mui/icons-material/QrCodeScannerRounded';
import FingerprintRoundedIcon from '@mui/icons-material/FingerprintRounded';
import VerifiedUserRoundedIcon from '@mui/icons-material/VerifiedUserRounded';
import AssuredWorkloadIcon from '@mui/icons-material/AssuredWorkload';
import { InputAdornment } from '@mui/material';



export interface Analysis {
    year: number
    valuation: string
    country: string[]
    industry: string[]
    supplier: string
    orgsize: string
    cloudmodel: string
    training: string
    remote: number | string
    insurance: string
    multifactor: string
    identityaccess: string
    securitymeasure: string[]
    confidence: string
}

export interface PropsSwitch {
    config: {
        title: string,
        icon: JSX.Element,
        subheader: (string | JSX.Element)[],
        name: string,
        items: Array<{ value: boolean, displayValue: string}>
    },
    handleChange: (name: string, values: string | number) => void,
    currentValue: string | number,
}


export interface PropsSelection {
    config: {
        title: string,
        icon: JSX.Element,
        subheader: (string | JSX.Element)[],
        label: string,
        name: string,
        items: Array<{ value: number | string, displayValue: string}>
    },
    handleChange: (name: string, values: string | number) => void,
    currentValue: string | number,
}

export interface PropsCheckbox {
    config: {
        title: string,
        icon: JSX.Element,
        subheader: (string | JSX.Element)[],
        label: string,
        items: Array<{ value: string, displayValue: string}>
    },
    handleChange: (name: string, values: string | number | string[]) => void,
    currentValue: string[],
}

export interface PropsRating {
    config: {
        title: string,
        icon: JSX.Element,
        subheader: (string | JSX.Element)[],
        label: string,
        switchLabel: string,
        items: { [index: string]: string },
    },
    handleChange: (name: string, values: number | string) => void,
    currentValue: string,
}


export interface PropsSlider {
    config: {
        title: string,
        icon: JSX.Element,
        subheader: (string | JSX.Element)[],
        name: string,
        min: number,
        max: number,
        step: number,
        adorment: JSX.Element,
    },
    handleChange: (name: string, values: number | string) => void,
    currentValue: number | string,
}


export interface PropsText {
    config: {
        title: string,
        icon: JSX.Element,
        subheader: (string | JSX.Element)[],
        name: string,
        label: string,
        adorment: JSX.Element,
    },
    handleChange: (name: string, values: string) => void,
    currentValue: string | number,
}

export const configurationsCards = {
    year: {
        title: "Year",
        icon: <AccessTime />,
        subheader: ["Enter the year for which you want to calculate the expected cost", <br/>, <br/>, <br/>],
        label: "Required",
        name: "year",
        items: [
            { value: 2012, displayValue: "2012"},
            { value: 2013, displayValue: "2013"},
            { value: 2014, displayValue: "2014"},
            { value: 2015, displayValue: "2015"},
            { value: 2016, displayValue: "2016"},
            { value: 2017, displayValue: "2017"},
            { value: 2018, displayValue: "2018"},
            { value: 2019, displayValue: "2019"},
            { value: 2020, displayValue: "2020"},
            { value: 2021, displayValue: "2021"},
            { value: 2022, displayValue: "2022"},
            { value: 2023, displayValue: "2023"},
            { value: 2024, displayValue: "2024"},
            { value: 2025, displayValue: "2025"},],
    },
    valuation: {
        title: "Valuation",
        icon: <MonetizationOnIcon />,
        subheader: ["Enter the equity value/ valuation of the company for which you want to determine the impact. The amount must be in USD.", <br/>, <br/>],
        label: "Valuation in USD",
        name: "valuation",
        adorment: <InputAdornment position="end">$</InputAdornment>
    },
    country: {
        title: "Country",
        icon: <PublicIcon />,
        subheader: ["Select all countries where physical offices with an internet connection are located. If your country is not on the list, select the geographically closest one.", <br/>],
        label: "country",
        items: [
            {value: "US", displayValue: "USA"},
            {value: "UK", displayValue: "United Kingdom"},
            {value: "GER", displayValue: "Germany"},
            {value: "FRA", displayValue: "France"},
            {value: "ITA", displayValue: "Italy"},
            {value: "CAN", displayValue: "Canada"},
            {value: "ESP", displayValue: "Spain"},
            {value: "SCA", displayValue: "Scandinavia"},
            {value: "TUR", displayValue: "Turkey"},
        ]
    },
    industry: {
        title: "Industry",
        icon: <FactoryIcon />,
        subheader: ["Select all industries in which the company operates in.", <br/>, <br/>, <br/>],
        label: "industry",
        items: [
            {value: "Banking", displayValue: "Banking"},
            {value: "Utilities", displayValue: "Utilities"},
            {value: "Aerospace and defence", displayValue: "Aerospace and Defence"},
            {value: "Software", displayValue: "Software"},
            {value: "Health", displayValue: "Health"},
            {value: "US Federal", displayValue: "US Federal"},
            {value: "Public sector", displayValue: "Public Sector"},
            {value: "Consumer goods", displayValue: "Consumer Goods"},
            {value: "Retail", displayValue: "Retail"},
            {value: "Life sciences", displayValue: "Life Sciences"},
            {value: "Communications and media", displayValue: "Communications and Media"},
            {value: "Travel", displayValue: "Travel"},
            {value: "Education", displayValue: "Education"},
            {value: "Automotive", displayValue: "Automotive"},
            {value: "Insurance", displayValue: "Insurance"},
            {value: "High tech", displayValue: "High Tech"},
            {value: "Capital markets", displayValue: "Capital Markets"},
            {value: "Energy", displayValue: "Energy"},
            {value: "Pharmaceuticals", displayValue: "Pharmaceuticals"},
            {value: "Industrial", displayValue: "Industrial"},
        ]
    },
    supplier: {
        title: "Supplier",
        icon: <ProductionQuantityLimitsIcon />,
        subheader: ["Rate the IT security and the trust in the company's suppliers. If the company has no supplier or their IT systems are not connected to the supplier IT: Set the switch to False"],
        label: "supplier",
        switchLabel: "Is your Supplier connected to your IT systems?",
        items: {
            0.5: 'Non existent',
            1: 'Very Bad',
            1.5: 'Poor',
            2: 'Poor+',
            2.5: 'Ok',
            3: 'Ok+',
            3.5: 'Good',
            4: 'Good+',
            4.5: 'Excellent',
            5: 'Excellent+',
          },
    },
    orgsize: {
        title: "Organization Size",
        icon: <GroupIcon />,
        subheader: ["Companies are classified according to the amount of full time employees: 1-10 = Micro; 10 - 49 = Small SME; 50 - 249 = Medium SME; >250 = Large SME. The rating aligns with OECD standards."],
        name: "orgsize",
        label: "Type",
        items: [
            { value: "Micro", displayValue: "Micro"},
            { value: "Small", displayValue: "Small"},
            { value: "Medium", displayValue: "Medium"},
            { value: "Large", displayValue: "Large"},
            { value: "No Specification", displayValue: "No Specification"},
        ],
    },
    cloudmodel: {
        title: "Cloud Model",
        icon: <CloudSyncRoundedIcon />,
        subheader: ["Choose the cloud model currently employeed at the firm. If no cloud model is used choose 'No Specification'.", <br/>, <br/>],
        name: "cloudmodel",
        label: "Model Type",
        items: [
            { value: "Public", displayValue: "Public"},
            { value: "Private", displayValue: "Private"},
            { value: "Hybrid", displayValue: "Hybrid"},
            { value: "No Specification", displayValue: "No Specification"},
        ],
    },
    training: {
        title: "Training",
        icon: <SchoolRoundedIcon />,
        subheader: ["Select whether the average employee has attended Cyber Security education. For instance a Phishing Mail Awareness course can count as Cyber Security training.", <br/>],
        name: "training",
        items: [
            { value: false, displayValue: "No Training"},
            { value: true, displayValue: "Training"},
        ],
    },
    remote: {
        title: "Remote Employees",
        icon: <ConnectWithoutContactRoundedIcon />,
        subheader: ["Enter how many employees work remotely. Check the box if the amount is unknown.", <br/>],
        name: "remote",
        min: 20,
        max: 100,
        step: 20,
        adorment: <InputAdornment position="end">%</InputAdornment>
    },
    insurance: {
        title: "Insurance",
        icon: <HistoryEduRoundedIcon />,
        subheader: ["Specify whether the company has any kind of cyber insurance.", <br/>, <br/>],
        name: "insurance",
        items: [
            { value: false, displayValue: "No Insurance"},
            { value: true, displayValue: "Insurance"}],
    },
    multifactor: {
        title: "Multifactor Authentication",
        icon: <QrCodeScannerRoundedIcon />,
        subheader: ["Specify if the company uses some kind of multifactor authentication to protect their IT systems. ", <br/>, <br/>],
        name: "multifactor",
        items: [
            { value: false, displayValue: "No Multifactor Auth"},
            { value: true, displayValue: "Multifactor Auth"}],
    },
    identityaccess: {
        title: "Identity Access Management",
        icon: <FingerprintRoundedIcon />,
        subheader: ["Specify if the company uses some kind of identity access management to protect their IT systems", <br/>, <br/>],
        name: "identityaccess",
        items: [
            { value: false, displayValue: "No Identity Access"},
            { value: true, displayValue: "Identity Access"}],
    },
    securitymeasure: {
        title: "Security Measures",
        icon: <VerifiedUserRoundedIcon />,
        subheader: ["Select all IT security measures which the company currently has fully deployed/operational", <br/>, <br/>],
        label: "securitymeasure",
        items: [
            {value: "Automated Checks & ML and AI", displayValue: "Automated Checks & ML and AI"},
            {value: "Cyber Analytics and Behavior Analytics", displayValue: "Cyber Analytics and Behavior Analytics"},
            {value: "Encryption Technologies", displayValue: "Encryption Technologies"},
            {value: "Risk management", displayValue: "Risk Management"},
            {value: "Sufficient Security Staff", displayValue: "Sufficient Security Staff"},
            {value: "Incident response plan testing", displayValue: "Incident Response Plan Testing"},
            {value: "Security intelligence systems", displayValue: "Security Intelligence Systems"},
            {value: "Advanced identity and access", displayValue: "Advanced Identity and Access"},
            {value: "Advanced Perimeter Controls", displayValue: "Advanced Perimeter Controls"},
            {value: "Data Loss prevention measures", displayValue: "Data Loss Prevention Measures"},
        ]
    },
    confidence: {
        title: "Confidence",
        icon: <AssuredWorkloadIcon />,
        subheader: ["Enter the level of confidence desired for the Value at Risk. Respectively, how confident do you want to be that your cost will not be higher than the cost displayed as Value at Risk?"],
        name: "confidence",
        label: "Confidence",
        items: [
            { value: "90", displayValue: "90%"},
            { value: "95", displayValue: "95%"},
            { value: "97.5", displayValue: "97.5%"},
            { value: "99", displayValue: "99%"},
        ],
    },


}


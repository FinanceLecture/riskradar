import React, { Dispatch, SetStateAction, useContext, useEffect, useState } from 'react'
import '../../styles/cybersurvey.css'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Box, Card, Checkbox, Input, Slider, ThemeProvider, Typography } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import {theme} from '../../theme/button'
import FormControl from '@mui/material/FormControl';
import { PropsSlider } from './types';




export const SliderCard = (props: PropsSlider): React.ReactElement  => {

      const [checked, setChecked] = useState<boolean>(false)

      const handleChangeSlider = (value: string | number | (string | number)[]) => {
        let x = value as number
        if(x< 20){
          x = 20
        }
        props.handleChange(props.config.name, x)
        
      }
    
      const handleSliderChange = (event: Event, newValue: number | number[]) => {
        handleChangeSlider(newValue)
      };
    
      const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = (event.target.value === '' ? 0 : Number(event.target.value));
        handleChangeSlider(value)
      };
    
      const handleBlur = () => {
        if (props.currentValue < 0) {
          handleChangeSlider(0)
        } else if (props.currentValue > 100) {
          handleChangeSlider(100)
        }
      };

    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    className='griditems-input'
                    variant="outlined">
                    <CardHeader
                        sx={{marginTop: checked ? null: "30px"}}
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.subheader} />
                    <CardContent>
                        <FormControl fullWidth>
                        <Box sx={{ marginLeft: "50px", marginTop: checked ? "-30px": "10px",marginBottom:"20px", width: "full", display: "flex", alignItems: "center" }}>
                            <Checkbox defaultChecked size="small" checked={checked} onChange={() => {
                                setChecked(!checked)

                                if(checked){
                                  props.handleChange(props.config.name, "No Specification")
                                } else {
                                    props.handleChange(props.config.name, 20)
                                }
                            }}/>
                            <Typography>The Number of Remote Workers is known</Typography>
                        </Box>
                        {checked ?
                        <Box sx={{ marginLeft: "50px", width: 300, display: "flex", alignItems: "center" }}>
                            <Slider
                                valueLabelDisplay="auto"
                                value={typeof props.currentValue === 'number' ? props.currentValue : 0}
                                onChange={handleSliderChange}
                                step={props.config.step}
                                min={props.config.min}
                                max={props.config.max}
                            />
                            <Input
                                sx={{ marginLeft: "20px"}}
                                endAdornment={props.config.adorment}
                                value={props.currentValue}
                                size="small"
                                onChange={handleInputChange}
                                onBlur={handleBlur}
                                inputProps={{
                                step: props.config.step,
                                min: props.config.min,
                                max: props.config.max,
                                type: 'number',
                                'aria-labelledby': 'input-slider',
                                }}
                            />
                        </Box> : null }
                        </FormControl>
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )


}
import React, { Dispatch, SetStateAction, useContext, useEffect } from 'react'
import '../../styles/cybersurvey.css'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Box, Card, Checkbox, FormControlLabel, SelectChangeEvent, Switch, ThemeProvider, Typography } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import {theme} from '../../theme/button'
import FormControl from '@mui/material/FormControl';
import { PropsSwitch } from './types';




export const SwitchCard = (props: PropsSwitch): React.ReactElement  => {

    const [checked, setChecked] = React.useState(false);
    const [checkedBox, setCheckedBox] = React.useState<boolean>(false)
    const [position, setPosition] = React.useState<number>(0)

    const getValue = () => {
        let first = props.config.items.find((item) => {
            return item.value === !checked;
        });

        return first? first.displayValue : props.currentValue
    } 

    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    className='griditems-input'
                    variant="outlined">
                    <CardHeader
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.subheader} />
                    <CardContent>
                        <Box sx={{ marginLeft: "50px", marginTop: checkedBox ? "-30px": "10px",marginBottom:"10px", width: "full", display: "flex", alignItems: "center" }}>
                            <Checkbox defaultChecked size="small" checked={checkedBox} onChange={() => {
                                setCheckedBox(!checkedBox)

                                if(checkedBox){
                                  props.handleChange(props.config.name, "No Specification")
                                } else {
                                  props.handleChange(props.config.name, props.config.items[position].displayValue)
                                }
                            }}/>
                            <Typography>{`I know if ${props.config.title} is used`}</Typography>
                        </Box>
                        {checkedBox ?
                        <FormControl fullWidth sx={{ marginLeft: "50px"}}>
                        <FormControlLabel
                            value="end"
                            control={<Switch color="primary" checked={checked} onChange={(e: SelectChangeEvent<string | number>) => {
                                props.handleChange(props.config.name, getValue())
                                setChecked(!checked)
                                setPosition(checked? 0: 1)
                            }}/>}
                            label={props.currentValue}
                            labelPlacement="end"
                            />
                        </FormControl>
                        : null}
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )


}
import React, { useState } from'react'
import '../../styles/cybersurvey.css'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Button, Card, createTheme, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import { AccessTime } from '@mui/icons-material';
import {theme} from '../../theme/cyberAnalyticsTheme'
import {SelectionCard} from './SelectionCard';
import { TextInput } from './TextInput';
import { Checkmarks } from './Checkmarks';
import { RatingCard } from './RatingCard';
import { SwitchCard } from './SwitchCard';
import { SliderCard } from './SliderCard';
import { Analysis, configurationsCards } from './types';
import { EvaluationType } from './Evaluation/EvaltuationTypes';
import { Evaluation } from './Evaluation/Evaluation';
import { useReduxState } from '../../redux/hooks';



const stringarray: string[] = []
const initial_analysis = {
    year: 2017,
    valuation: "70000000",
    country: stringarray,
    industry: stringarray,
    supplier: "No Specification",
    orgsize: "No Specification",
    cloudmodel: "No Specification",
    training: "No Specification",
    remote: "No Specification",
    insurance: "No Specification",
    multifactor: "No Specification",
    identityaccess: "No Specification",
    securitymeasure: stringarray,
    confidence: "95"
} as Analysis

export const CyberSurvey = () => {

    const serverURL = useReduxState((state: { server: any; }) => state.server);
    const [evaluation, setEvaluation] = useState<EvaluationType | null>(null)
    const [analysis, setAnalysis] = useState<Analysis>(initial_analysis)
    const re = /^[0-9\b]+$/

    const handleChange = (name: string, values: string | number | string[]): void => {
        setAnalysis({
            ...analysis,
            [name] : values
        });
    }

    const handleInputChange = (name: string, values: string): void => {

        if(String(values).length <= 12 && (values == '' || re.test(values))){
            handleChange(name, values)
        }
    }

    const handleRatingChange = (name: string, values: number| string): void => {
        if(typeof values === 'string'){
            handleChange(name, values)
        } else{
            values > 2.5 ? handleChange(name, "Supplier Safe") : handleChange(name, "Supplier Breach")
        }
    }


    const handleEstimate = (analysis: Analysis) => {
        const estimateRequest = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(analysis)
        };

        const fetchData = async () => {
            try {
                const response = await fetch(`${serverURL.baseURL}${serverURL.estimateGET}`, estimateRequest);
                const result = await response.json();
                if (response.ok) {
                   console.log(result['message'])
                   console.log(result['data'])

                   setEvaluation(result['data'])
                }
            } catch (error: any) {
                console.log('Estimation Failed', error)
            }

        }
        fetchData();

    }

  
    return(
        <div>
            <div className='animationText'>
            <h1>Determine the Impact!</h1>
            </div>
            <Grid sx={{ justifyContent: 'center', padding: '80px', backgroundColor: "Cornsilk"}} container spacing={{ xs: 2, md: 3}} columns={{ xs: 4, sm: 8, md: 12 }}>
                <SelectionCard 
                    config={configurationsCards.year}
                    handleChange={handleChange}
                    currentValue = {analysis.year}
                />
                <TextInput
                    config={configurationsCards.valuation}
                    handleChange={handleInputChange}
                    currentValue={analysis.valuation} 
                />
                <Checkmarks 
                    config={configurationsCards.country}
                    handleChange={handleChange}
                    currentValue={analysis.country} 
                />
                <Checkmarks 
                    config={configurationsCards.industry}
                    handleChange={handleChange}
                    currentValue={analysis.industry} 
                />
                <RatingCard 
                    config={configurationsCards.supplier}
                    handleChange={handleRatingChange}
                    currentValue={analysis.supplier} 
                />
                <SelectionCard
                    config={configurationsCards.orgsize}
                    handleChange={handleChange}
                    currentValue={analysis.orgsize} 
                />
                <SelectionCard
                    config={configurationsCards.cloudmodel}
                    handleChange={handleChange}
                    currentValue={analysis.cloudmodel} 
                />
                <SwitchCard 
                    config={configurationsCards.training}
                    handleChange={handleChange}
                    currentValue={analysis.training} 
                />
                <SliderCard 
                       config={configurationsCards.remote}
                       handleChange={handleChange}
                       currentValue={analysis.remote} 
                />
                <SwitchCard 
                        config={configurationsCards.insurance}
                        handleChange={handleChange}
                        currentValue={analysis.insurance} 
                    />
                <SwitchCard 
                        config={configurationsCards.multifactor}
                        handleChange={handleChange}
                        currentValue={analysis.multifactor} 
                    />
                <SwitchCard 
                        config={configurationsCards.identityaccess}
                        handleChange={handleChange}
                        currentValue={analysis.identityaccess} 
                    />
                <Checkmarks 
                    config={configurationsCards.securitymeasure}
                    handleChange={handleChange}
                    currentValue={analysis.securitymeasure} 
                />
                <SelectionCard
                    config={configurationsCards.confidence}
                    handleChange={handleChange}
                    currentValue={analysis.confidence} 
                />
            </Grid>
            <div style={{ backgroundColor: "black", display: "flex", 
            flexDirection: "row", justifyContent: "center", alignItems: "center", height: "200px"}}>
                <ThemeProvider theme={theme}>
                    <Button variant="outlined" onClick={() => {
                        handleEstimate(analysis)
                    }}>Evaluate Impact</Button>
                </ThemeProvider>
            </div>
            { evaluation != null ?
            <Evaluation evaluation={evaluation} analysis={analysis}/> 
            : null
            }
            
        </div>
        

    )
}


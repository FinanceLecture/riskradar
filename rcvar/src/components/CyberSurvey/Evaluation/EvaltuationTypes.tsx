import React from 'react'
import VerticalAlignBottomIcon from '@mui/icons-material/VerticalAlignBottom';
import VerticalAlignTopIcon from '@mui/icons-material/VerticalAlignTop';
import StarHalfIcon from '@mui/icons-material/StarHalf';
import DonutLargeIcon from '@mui/icons-material/DonutLarge';
import TrendingDownIcon from '@mui/icons-material/TrendingDown';
import TroubleshootIcon from '@mui/icons-material/Troubleshoot';
import EmojiObjectsIcon from '@mui/icons-material/EmojiObjects';
import WaterfallChartIcon from '@mui/icons-material/WaterfallChart';



export interface EvaluationType {
    best_security_measure: { name: string, value: string }
    company_security: number
    composition: Array<{name: string, value: number}>
    ev_estimator: number
    ev_model: number
    image: string
    max_value: number
    min_value: number
    negative: Array<{name: string, value: number}>
    percentage: number
    positive: Array<{name: string, value: number}>
    var: number
}


export interface PropsDonut {
    config: {
        color: {
            red: () => number 
            green: () => number 
            blue: () => number 
        }
        icon: JSX.Element
        title: string
        subheader: (string | JSX.Element)[]
    },
    data: Array<{ name: string, value: number}>
}

export interface PropsScoring {
    config: {
        icon: JSX.Element
        title: string
    },
    data: { 
        currentCost: number,
        percentageCurrent: number,
        min: number,
        max: number,
    }
}

export interface PropsNumber {
    config: {
        icon: JSX.Element
        title: string
        subheader: string
        unit: string
    },
    data: number | string,
    year: number
}

export interface PropsImage {
    config: {
        icon: JSX.Element
        title: string
    },
    data: string,
    confidence: string,
}

export const evalconfig = {
    decrease: {
         color: {
        red: () => {return Math.floor(Math.random() * (150 - 60) + 60)},
           blue:  () => {return Math.floor(Math.random() * (50 - 0) + 0)},
           green: () => {return Math.floor(Math.random() * (255 - 100) + 100)}
         },
         icon: <VerticalAlignBottomIcon sx={{ color: "green",  fontSize: 40 }}/>,
         title: "Factors Which Decrease Cost",
         subheader: ["The Chart below shows all factors which decrease the expected cost. The percentage shows the contribution of the business characterstic to the total cost reduction."]
    },
    increase: {
        color: {
           red: () => {return Math.floor(Math.random() * (255 - 190) + 153)},
           blue:  () => {return Math.floor(Math.random() * (102 - 0) + 0)},
           green: () => {return Math.floor(Math.random() * (102 - 0) + 0)}
        },
        icon: <VerticalAlignTopIcon sx={{ color: "red",  fontSize: 40 }}/>,
        title: "Factors Which Increase Cost",
        subheader: ["The Chart below shows all factors which increase the expected cost. The percentage shows the contribution of the business characterstic to the total cost increase."]
   },
   costcomponent: {
    color: {
       red: () => 100,
       blue:  () => 255,
       green: () => {return Math.floor(Math.random() * 255)}
    },
    icon: <DonutLargeIcon sx={{ color: "blue", fontSize: 40 }}/>,
    title: "Composition of Expected Cost",
    subheader: ["This Donut Chart shows how the possible cost are composed.", <br/>, <br/>]
    },
    scoring: {
        icon: <StarHalfIcon sx={{ color: "orange", fontSize: 40 }}/>,
        title: "Security Score",
    },
    evestimator: {
        icon: <TrendingDownIcon sx={{ color: "red",  fontSize: 40 }}/>,
        title: "Expected Cost Prediction",
        subheader: "This value represents the predicted total cost (direct, indirect and opportunity) of all cyber incidents for the year ",
        unit: "$",
   },
   evmodel: {
    icon: <TrendingDownIcon sx={{ color: "red",  fontSize: 40 }}/>,
    title: "AI Model Expected Cost Prediction",
    subheader: "This value represents the predicted total cost by the Federated Learning model for the year ",
    unit: "$",
   },
    var: {
    icon: <TroubleshootIcon sx={{ color: "orange", fontSize: 40 }}/>,
    title: "Value at Risk",
    subheader: "This value shows the risk of cyber incidents in the form of the Value at Risk for the year ",
    unit: "$",
    },
    securitymeasure: {
    icon: <EmojiObjectsIcon sx={{ color: "green",  fontSize: 40 }}/>,
    title: "Most Efficient Security Action",
    subheader: "The security measure mentioned below shows the action, which has the highest decreasing impact on the cost.",
    unit: "",
    },
    image: {
        icon: <WaterfallChartIcon sx={{ color: "blue",  fontSize: 40 }}/>,
        title: "Risk Distribution",
    },
}
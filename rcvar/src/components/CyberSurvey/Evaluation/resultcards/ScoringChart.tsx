import React from 'react'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Card, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import {theme} from '../../../../theme/evaluationTheme'
import GaugeChart from 'react-gauge-chart';
import { PropsScoring } from '../EvaltuationTypes';



export const ScoringChart = (props: PropsScoring): React.ReactElement  => {

     
    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    variant="outlined">
                    <CardHeader
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={`This shows the min ($ ${props.data.min}) and max ($ ${props.data.max}) range which is achievable through security measures, training, insurance, multifactor auth and identity access systems.`} />
                    <CardContent sx={{display: "flex", justifyContent: "center", flexDirection: "column"}}>
                    <GaugeChart id="gauge-chart5"
                        nrOfLevels={1000}
                        arcsLength={[0.33, 0.33, 0.33]}
                        colors={['#5BE12C', '#F5CD19', '#EA4228']}
                        formatTextValue={(value: string) => {return `$ ${props.data.currentCost}`}}
                        percent={props.data.percentageCurrent}
                        animate
                        animateDuration={2000}
                        textColor='black'
                        needleColor='#808080'
                        needleBaseColor='black'
                        arcPadding={0.02}
                        />
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )
}


import React, { useState } from 'react'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Box, Card, createTheme, DialogContentText, Divider, IconButton, TextField, ThemeProvider, Typography } from '@mui/material';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import CardContent from '@mui/material/CardContent';

import GaugeChart from 'react-gauge-chart';
import { PropsNumber, PropsScoring } from '../EvaltuationTypes';

const theme = createTheme({
    components: {
        // Name of the component
        MuiCard: {
            styleOverrides: {
                root: {
                    width: '500px',
                    height: '200px',
                    display:'flex',
                    justifyContent:'center',
                    flexDirection: 'column',
                }
            }
          },
    }
  });

export const NumberChart = (props: PropsNumber): React.ReactElement  => {

    const [open, setOpen] = useState<boolean>(false)

    const handleDialog = (): void => {
        setOpen(!open)
    }


    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    variant="outlined">
                    <CardHeader
                        avatar={props.config.icon}
                        action={
                            props.config.title == "Value at Risk" ?
                            <IconButton>
                                <HelpOutlineIcon onClick={handleDialog} />
                                <DialogVaR open={open} handleDialog={handleDialog}/>
                            </IconButton> : null

                        }
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.title == "Most Efficient Security Action" ? props.config.subheader: props.config.subheader+props.year +"."} />
                    <CardContent sx={{display: "flex", justifyContent: "center", flexDirection: "column", height: "175px"}}>
                    <Typography variant="h4" align="center" sx={{opacity: 0.4}}>
                        {`${props.config.unit} ${props.data}`}
                    </Typography>
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )
}


export const DialogVaR = (props: {open: boolean, handleDialog: () => void}): React.ReactElement  => {
  
    return (
        <Dialog open={props.open} onClose={props.handleDialog}>
        <DialogTitle>Value at Risk
        <IconButton
          onClick={props.handleDialog}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {<div><p>The Value at Risk is a risk measure which originated in the financial industry. The Risk Distribution graph below shows the cost in relation to their occurence. 
            If the cost of the x-percentile is 1000 USD, we can say that with a certain confidence the final cost will not exceed this amount for the year 2019. In this example the 1000 USD represent the Cyber Value at Risk.
            The Cyber Value at Risk has three dimensions. First of all the monetary value, which changes depending on the input characterstics of your firm. Secondly, the confidence level, which is per default 95% but can be adjusted above as well.
            Last dimension is the time and can be changed in the first tile of the input mask. The length of the timeframe, so months instead of years, can unfortunatly not be changed. To reiterate the example from above:</p>
            <p>If the Cyber Value at Risk is 1000 USD I can say that with a confidence x, that my cost will not exceed 1000 USD in the year 2019. </p>

            <p>Another point that is important to keep in mind while interpreting the value is that during the development of the distribution, the variance is determined to consistent.
            This means that the Cyber Value at Risk changes with every input specification since it is depended on the expected value. But the variance of the distribution does not change.
            Therefore, the distribution is just a linear transformation which means that the CVaR is always 2.9 times the expected value. 
            Nevertheless, the informations displayed together with the publicly available prices for security measures should allow for a constructive optimization discussions along the dimensions: investment, risk and expected cost.</p>
            </div>}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        </DialogActions>
      </Dialog>
    );
  }
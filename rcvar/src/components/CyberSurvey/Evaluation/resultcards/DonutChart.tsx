import React, { Dispatch, PureComponent, SetStateAction, useContext, useEffect, useState } from 'react'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Card, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import FingerprintRoundedIcon from '@mui/icons-material/FingerprintRounded';
import {theme} from '../../../../theme/evaluationTheme'
import '../../../../styles/cybersurvey.css'
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import { PropsDonut } from '../EvaltuationTypes';
import { render } from 'react-dom';


interface Dictionary<T> {
    [Key: string]: T;
  }




export const DonutChart = (props: PropsDonut): React.ReactElement  => {
    
    const[colors, setColors] = useState<Array<string>>([])
    const[charsValues, setCharsValues] = useState<string>("")

    useEffect(() => {
        let temp: Array<string> = []
        let totalValueString: string = ""

        props.data.forEach((element: {name: string; value: number}) => {
            temp.push(dynamicColors())
            totalValueString = totalValueString + element.name
        })
        setColors(temp)
        setCharsValues(totalValueString)
    }, [])

    const renderLabel = (entry: { name: string, value: number}) => {
        return `${entry.value/ 10} %`;
    }

    const componentToHex = (c:number) => {
        let hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
      
    const dynamicColors = ():string => {
        return "#" + componentToHex(props.config.color.red()) + componentToHex(props.config.color.green()) + componentToHex(props.config.color.blue());
     };



     
    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    variant="outlined">
                    <CardHeader
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={props.config.subheader} />
                    <CardContent sx={{display: "flex", justifyContent: "center", flexDirection: "column", marginTop: "-20px"}}>
                    <ResponsiveContainer className="container" height={300} width='100%'>
                    <PieChart>
                        <Pie
                        label={renderLabel}
                        isAnimationActive={false}
                        data={props.data}
                        cx={234}
                        cy={120}
                        innerRadius={60}
                        outerRadius={90}
                        fill="#8884d8"
                        paddingAngle={5}
                        dataKey="value"
                        >
                        {props.data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={colors[index]} />
                        ))}
                        </Pie>
                        <Tooltip formatter={(value: any, name, props) => `${value /10}%`}/>
                        { props.data.toString().length > 78 ? null :
                        <Legend
                        payload={
                            props.data.map(
                            (item, index) => ({
                                id: item.name,
                                type: "square",
                                value: `${item.name} (${item.value/10}%)`,
                                color: colors[index]
                            })
                            )
                        }
                        />}
                    </PieChart>
                    </ResponsiveContainer>
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )
}


import React, { Dispatch, PureComponent, SetStateAction, useContext, useEffect, useState } from 'react'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Card, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';

import {theme} from '../../../../theme/evaluationTheme'
import { PropsImage } from '../EvaltuationTypes';




export const PictureChart = (props: PropsImage): React.ReactElement  => {

    
    return(
        <Grid item>
            <ThemeProvider theme={theme}>
                <Card
                    variant="outlined">
                    <CardHeader
                        sx={{ marginTop: "20px", zIndex: "10"}}
                        avatar={props.config.icon}
                        titleTypographyProps={{ variant: 'h6' }}
                        title={props.config.title}
                        subheader={`This graph shows the cost distribution of the described company. The green area depicts the cost which occurs with ${props.confidence}% while blue represents cost in ${100-parseFloat(props.confidence)}% of the cases.`} />
                    <CardContent sx={{display: "flex", justifyContent: "center", flexDirection: "column", marginTop: "-55px"}}>
                        <img src={`data:image/jpeg;base64,${props.data}`} />
                    </CardContent>
                </Card>
            </ThemeProvider>
        </Grid>
    )
}


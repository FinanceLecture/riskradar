import { Grid } from '@mui/material'
import React from 'react'
import { DonutChart } from './resultcards/DonutChart';
import { evalconfig } from './EvaltuationTypes';
import { ScoringChart } from './resultcards/ScoringChart';
import { NumberChart } from './resultcards/NumberChart';
import { EvaluationType } from './EvaltuationTypes';
import { PictureChart } from './resultcards/PictureChart';
import { Analysis } from '../types';

export const Evaluation = (props: {evaluation: EvaluationType | null, analysis: Analysis}): React.ReactElement => {
    return(
        <Grid sx={{ justifyContent: 'center', padding: '80px', backgroundColor: "Cornsilk" }} container spacing={{ xs: 2, md: 3}} columns={{ xs: 4, sm: 8, md: 12 }}>
              {(props.evaluation && props.evaluation.negative.length > 0) ? <DonutChart
                    config={evalconfig.decrease}
                    data={props.evaluation.negative}
              /> : null }
              {(props.evaluation && props.evaluation.positive.length > 0) ? <DonutChart
                    config={evalconfig.increase}
                    data={props.evaluation.positive}
              /> : null }
              {(props.evaluation && props.evaluation.composition.length > 0) ? <DonutChart
                    config={evalconfig.costcomponent}
                    data={props.evaluation.composition}
              /> : null } 
              {(props.evaluation && props.evaluation.company_security && props.evaluation.percentage && props.evaluation.min_value && props.evaluation.max_value) ? 
              <ScoringChart 
                    config={evalconfig.scoring}
                    data={{
                        currentCost: props.evaluation.company_security,
                        percentageCurrent: props.evaluation.percentage,
                        min: props.evaluation.min_value,
                        max: props.evaluation.max_value,
                    }}
              /> : null},
              { (props.evaluation && props.evaluation.ev_estimator) ?
                <NumberChart 
                    config={evalconfig.evestimator} 
                    data={props.evaluation.ev_estimator}
                    year={props.analysis.year}                />
                : null
              },
              { (props.evaluation && props.evaluation.var) ?
                <NumberChart 
                    config={evalconfig.var} 
                    data={props.evaluation.var}  
                    year={props.analysis.year}                />
                : null
              },
              { (props.evaluation && props.evaluation.ev_model) ?
                <NumberChart 
                    config={evalconfig.evmodel} 
                    data={props.evaluation.ev_model}  
                    year={props.analysis.year}                />
                : null
              },
              { (props.evaluation && props.evaluation.best_security_measure) ?
                <NumberChart 
                    config={evalconfig.securitymeasure} 
                    data={props.evaluation.best_security_measure.name}  
                    year={props.analysis.year}                />
                : null
              },
              { (props.evaluation && props.evaluation.image) ?
                <PictureChart 
                    config={evalconfig.image} 
                    data={props.evaluation.image}  
                    confidence={props.analysis.confidence}              />
                : null
              },           
        </Grid>
    )
}

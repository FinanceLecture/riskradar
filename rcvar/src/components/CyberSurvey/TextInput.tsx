import React from 'react'
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import { Card, Input, InputLabel, ThemeProvider } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import { AccessTime } from '@mui/icons-material';
import {theme} from '../../theme/button'
import { PropsText } from './types'




export const TextInput = (props: PropsText): React.ReactElement => {


    return(
        <Grid item>
                <ThemeProvider theme={theme}>
                    <Card 
                    className='griditems-input'
                    variant="outlined">
                    <CardHeader
                            avatar={
                                props.config.icon
                            }
                            titleTypographyProps={{variant:'h6' }}
                            title={props.config.title}
                            subheader={props.config.subheader}
                        />
                     <CardContent>
                     <InputLabel htmlFor="standard-adornment-password">{props.config.label}</InputLabel>
                     <Input
                        fullWidth
                        required
                        id="outlined-required"
                        type="text"
                        onChange={(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
                            props.handleChange(props.config.name, e.target.value)
                        }}
                        endAdornment={props.config.adorment}
                        value={props.currentValue}
                        />
                     </CardContent>
                    </Card>
                    </ThemeProvider>
                </Grid>
    )
}


import React from 'react'
import '../../styles/homepage.css'



export default function Footer() {

    return (
        <div style={{width: "100%", display:'flex', backgroundColor: 'black', justifyContent: 'center', textAlign: "center", color: 'white', fontFamily: 'Arial'}}>
                <div style={{width: '40%', margin: "40px"}}>
                    <h3>Why worry about Cyber Risk?</h3>
                    <p style={{ lineHeight: "30px", textAlign: 'justify'}}>
                        The FBI's annual internet crime report for the year 2020 documented losses of over 4.1 billion USD. A simple Phishing Mail can disrupt a firms production and damage its reputation. Often underestimated are also opportunity costs occuring due to an attack. To effectively conduct cyber risk management it is important to know the potential costs. This tool, developed especially for Small and Middle Size Enterprises, helps your company to do the next step in your company's IT security management. So that you can sleep well at night.
                    </p>
        </div>
            

           
        </div>
    )
}

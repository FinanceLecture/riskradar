import React, { useRef } from'react'
import Footer from './Footer';
import GrootAnimation from './Video';
import Youtube from './Youtube';
import TextAnimation from './TextAnimation';




export const Home = () => {

    const LearnMoreRef = useRef(null)

    const scrollToLearnMore = (ref: any ) => window.scrollTo({top: ref.current.offsetTop, left: 0, behavior: 'smooth'})   


    return(
        <>
        <TextAnimation scrollToFooter={scrollToLearnMore} reference={LearnMoreRef}/>
        <GrootAnimation/>
        <Youtube />
        <div ref={LearnMoreRef}>
            <Footer/>
        </div>
    </>
    
    )
}
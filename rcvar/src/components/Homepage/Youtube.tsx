import React from 'react'
import ReactPlayer from "react-player"

export default function Youtube() {
  return (
    <div className='youtubeBackground'>
      <div className='youtubeContainer'>
        <ReactPlayer
          url="https://youtu.be/f94RkOteXgI"
        />
      </div>
    </div>
  )
}
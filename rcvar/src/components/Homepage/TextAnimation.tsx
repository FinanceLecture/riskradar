import React from 'react';
import { Button, ThemeProvider } from '@mui/material'
import Typewriter from "typewriter-effect";
import '../../styles/homepage.css'
import {theme} from '../../theme/button'
import { useNavigate } from 'react-router-dom';
import { useReduxState, useSetReduxState } from '../../redux/hooks';
import { setUrl } from '../../redux/reducers/navigationbarReducer';

function TextAnimation(props: {scrollToFooter: (ref: any) => void, reference: any }) {

    let navigate = useNavigate();
    const dispatch = useSetReduxState()
    const urls = useReduxState((state) => state.urls);
  
    return (
      <ThemeProvider theme={theme}>
        <div className='animationText'>
        <h1><Typewriter onInit={
        (typewriter) => {
          typewriter
            .typeString("Manage your Cyber Risks before they occur.")
            .pauseFor(2000)
            .deleteAll()
            .typeString("Estimate the financial impact today!")
            .start()
        }
      }/></h1>
      <div className='textAnimation-buttoncontainer'>
      
        <Button className='home-page-button' variant="outlined" size="large" onClick={() => {navigate(urls.cyberRisk);dispatch(setUrl(urls.cyberRisk))}}>Get Started</Button>
        <Button variant="outlined" size="large" onClick={() => {props.scrollToFooter(props.reference)}}>Learn More</Button>
      </div>
    </div>
    </ThemeProvider>
    );
  }
  
export default TextAnimation;
    
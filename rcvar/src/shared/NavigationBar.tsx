import React from 'react';
import { useNavigate } from "react-router-dom";
import { useReduxState, useSetReduxState } from '../redux/hooks';
import { ThemeProvider } from '@mui/material';
import {theme} from '../theme/button'
import '../styles/navigationbar.css';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { setUrl } from '../redux/reducers/navigationbarReducer';


interface Dictionary<T> {
    [Key: string]: T;
  }
  


export const NavigationBar = (): React.ReactElement => {

    const urls = useReduxState((state) => state.urls);
    const nav = useReduxState((state) => state.nav);
    const dispatch = useSetReduxState()
    let home = urls.home
    let cyberRisk = urls.cyberRisk

    
    const number2url: Dictionary<string> = {
        0: home,
        1: cyberRisk,
      };
    
      const url2number :  Dictionary<number> = {};
      url2number[home] = 0
      url2number[cyberRisk] = 1
 
    
    
      let navigate = useNavigate();

    
      const handleChange = (event: React.SyntheticEvent, newValue: number) => {
     
        navigate(number2url[newValue])
        dispatch(setUrl(number2url[newValue]))

      };



    return(
      <ThemeProvider theme={theme}>
          <Box className='center_horizontally' sx={{ maxWidth: { xs: "100%", sm: "100%" }, bgcolor: 'black', position: 'absolute'}}>
          <Tabs
            value={url2number[nav.currentURL]}
            onChange={handleChange}
            variant="scrollable"
            scrollButtons
            allowScrollButtonsMobile
            aria-label="scrollable force tabs example">
              
                  <Tab label="Home"/>
                  <Tab label="Risk Radar" />
                </Tabs>
          </Box>
        </ThemeProvider>
        
    )
}

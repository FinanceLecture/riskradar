import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { NavState, URLstate } from '../types';
import type { RootState } from '../store'

const initialState: NavState = {
    show: true,
    currentURL: "/home",
} as NavState

export const navSlice = createSlice({
    name: 'Nav',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {
        setShow: (state, action: PayloadAction<boolean>) => {
            state.show = action.payload
        },
        setUrl: (state, action: PayloadAction<string>) => {
            state.currentURL = action.payload
        },
    }
})

export const { setShow, setUrl } = navSlice.actions

// Other code such as selectors can use the imported `RootState` type
// export const selectHomeUrl = (state: RootState) => state.urls.home
// export const selectCyberRiskURL = (state: RootState) => state.urls.cyberRisk
// export const selectDocumentation = (state: RootState) => state.urls.documentation
// export const selectFederatedLearning = (state: RootState) => state.urls.federatedLearning

export default navSlice.reducer
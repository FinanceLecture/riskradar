import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { URLstate } from '../types';
import type { RootState } from '../store'

const initialState: URLstate = {
    home: "/home",
    cyberRisk: "/cyber_risk",
    documentation: "/documentation",
    federatedLearning: "/federated_learning",
    pdf: "/document/pdf",
} as URLstate

export const urlSlice = createSlice({
    name: 'URL',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {}
})


// Other code such as selectors can use the imported `RootState` type
// export const selectHomeUrl = (state: RootState) => state.urls.home
// export const selectCyberRiskURL = (state: RootState) => state.urls.cyberRisk
// export const selectDocumentation = (state: RootState) => state.urls.documentation
// export const selectFederatedLearning = (state: RootState) => state.urls.federatedLearning

export default urlSlice.reducer
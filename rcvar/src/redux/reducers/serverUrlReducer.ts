import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ServerState } from '../types';
import type { RootState } from '../store'



const initialState: ServerState = {
    baseURL: "http://localhost:5000",
    productionURL: "http://0.0.0.0:5000",
    estimateGET: `/get_estimation`,
   
} as ServerState

const ServerSlice = createSlice({
    name: 'ServerURL',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {}
})


// Other code such as selectors can use the imported `RootState` type
// export const selectHomeUrl = (state: RootState) => state.urls.home
// export const selectCyberRiskURL = (state: RootState) => state.urls.cyberRisk
// export const selectDocumentation = (state: RootState) => state.urls.documentation
// export const selectFederatedLearning = (state: RootState) => state.urls.federatedLearning

export default ServerSlice.reducer
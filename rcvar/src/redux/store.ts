import { configureStore } from '@reduxjs/toolkit';
import {
  FLUSH, PAUSE,
  PERSIST, persistReducer, persistStore, PURGE,
  REGISTER, REHYDRATE
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import urlReducers from './reducers/urlReducers';
import serverReducers from './reducers/serverUrlReducer';
import navReducers from './reducers/navigationbarReducer';


const persistConfig = {
  key: 'persistor',
  storage,
}
 
const persistedReducerURL = persistReducer(persistConfig, urlReducers)
const persistedReducerServer = persistReducer(persistConfig, serverReducers)
const persistedNav = persistReducer(persistConfig, navReducers)


export const store = configureStore({
  reducer: {
    urls: persistedReducerURL,
    server: persistedReducerServer,
    nav: persistedNav,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
})

export const persistor = persistStore(store)

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

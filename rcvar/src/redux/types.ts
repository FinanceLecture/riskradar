export interface URLstate{
    home: string 
    cyberRisk: string 
    documentation: string 
    federatedLearning: string
    pdf: string
}

export interface ServerState{
    baseURL: string
    productionURL: string
    estimateGET: string 
}

export interface NavState{
    show: boolean,
    currentURL: string,
}

  